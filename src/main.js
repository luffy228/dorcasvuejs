import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import store from "./store";
import dashboard from "./components/dashboard.vue";
import auth from "./components/auth.vue";
import Institution from "./components/Institution.vue";
import VueRouter from "vue-router";

import Vuex from "vuex";

window.Vue = require('vue');

Vue.use(Vuex);




Vue.use(VueRouter)

Vue.config.productionTip = false;




 const routes =[
  {
    path: "/",
    component: auth
  },
  {
    path: "/dashboard",
    component: dashboard
  },
  {
    path: "/Institution",
    component: Institution
  }
]



 const router = new VueRouter({
  routes,
  mode: 'history'
})



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
